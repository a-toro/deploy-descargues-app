const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const DescargueSchema = new Schema({
    nombre_tienda: {
        type: String,
        required: true
    },
    fecha_companamiento: {
        type: Date,
        required: true,
    },
    hora_inicio_acomp: {
        type: String,
        required: true
    },
    nombre_conductor: {
        type: String,
        required: true
    },
    placa_vehiculo: {
        type:String,
        required: true
    },
    sello_entrada: {
        type: String
    },
    sello_salida: {
        type: String
    },
    hora_fin_acomp: {
        type:String,
        required: true
    },
    nombre_supervisor: {
        type: String,
        required: true
    },
    fecha_registro: {
        type: Date,
        default: Date.now
    },
});

const Descargue = mongoose.model('Descargue', DescargueSchema, "Descargues");
module.exports =  Descargue;