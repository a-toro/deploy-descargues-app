const modelDescargue = require('./model');

module.exports.getDescargues = (req, res) => {
    try {
        modelDescargue.find({} , (error, docs) =>{
            if(error){
                res.send({'error': error});
            } else{
                res.send(docs);
            }
        })
    } catch (error) {
        res.send({'error': error})
    }
}

module.exports.getDescargue = (req, res) => {
    const id = req.params.id;
    try {
        modelDescargue.find({"id": id} , (error, docs) =>{
            if(error){
                res.send({'error': error});
            } else{
                res.send(docs);
            }
        })
    } catch (error) {
        res.send({'error': error})
    }
}

module.exports.createDescargue = (req, res) =>{
    const datos = req.body;
    try {
        modelDescargue.create(datos, (error, docs) => {
            if(error){
                res.send(error);
            } else {
                res.send(docs);
            }
        })
    } catch (error) {
        console.log({error});
    }
}

module.exports.updateDescargue = (req, res) => {
    const id = req.params.id;
    const datos = req.body;

    try {
        modelDescargue.updateOne({id_producto:id}, datos, (error, docs) =>{
            if(error){
                res.send(error);
            } else {
                res.send(docs);
            }
        });
    } catch (error) {
        res.send(error);
    }
}

module.exports.deleteDescargue = (req, res) => {
    const id = req.params.id;
    try {
        modelDescargue.deleteOne({id_producto:id}, (error, docs) => {
            if(error){
                res.send(error);
            }else{
                res.send(docs);
            }
        })
    } catch (error) {
        console.log(error);
    }
}