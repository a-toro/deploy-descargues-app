const express = require('express');
const routes = express.Router();

const serviciosDescargue = require('./service');

routes.get('/', serviciosDescargue.getDescargues);
routes.get('/:id', serviciosDescargue.getDescargue);
routes.post('/crear', serviciosDescargue.createDescargue);
routes.put('/editar/:id', serviciosDescargue.updateDescargue);
routes.delete('/eliminar/:id', serviciosDescargue.deleteDescargue);

module.exports = routes;