const express = require('express');
const cors = require('cors');
const mongoose = require('mongoose');
const path = require('path');
require('dotenv').config();

// Api config
const app = express();
const port = process.env.PORT;
app.use(express.json());
app.use(express.urlencoded({extended: true}))
app.use(cors());

// Controller Descragues
const controllerDescargues = require('./descargues/controller');

// Configuirar carpeta publica
const publicPath = path.resolve(__dirname, 'public');
app.use(express.static(publicPath));

app.use('/api/descargues', controllerDescargues)

app.get('/', (req, res) => {
    res.sendFile(path.join(__dirname + './index.html'));
})

// Database connection
const url_db = process.env.URL_DATABASE_CON;

mongoose.connect(
        url_db,
        {useNewUrlParser: true, useUnifiedTopology: true}
    )
    .then(result => app.listen(port, ()=> {console.log("App running");}))
    .catch( error => console.log(error))